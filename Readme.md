# Django Portfolio
**Introduction**: This Django application Is a portfolio that contains a landing page and blog

**Requirements**:
- Operating System: Linux (Ubuntu 18.04)
- Python v.3.6.9
- Pip v9.0.1
- Django v2.2.9



## System Configuration

Ensure that Python3 is installed:
```
$ python3 --version
```
Expected output: 
```
$ Python 3.6.9
```
Ensure pip3 is installed:
```
$ pip3 --version
```
Expected output: 
```
$ pip 9.0.1
```
## Installation
Create a Virtual Environment:
```
$ virtualenv venv
```

Start the Virtual Environment: 
```
$ source venv/bin/activate 
```
>to deactivate the virtual environment
>```
>(venv)$ deactivate
>``` 

Clone the repository: 
```
$ git clone https://adamtlee1@bitbucket.org/adamtlee1/portfolio.git
```
Install Django 2.2.9 (Or 2.2.*)
```
$ pip install Django==2.2.*
```
Install psycopg2:
```
$ pip install psycopg2-binary
```
Install Pillow:
```
pip install Pillow
```
Move into the project directory:
```
$ cd portfolio
```
Run the Application: 
```
python3 manage.py runserver
```
Navigate to localhost:8000